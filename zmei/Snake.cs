﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zmei
{
    public partial class Snake : Form
    {
        public class coord
        {
            public int X;
            public int Y;
            public coord(int x, int y)
            {
                X = x;
                Y = y;
            }
        }


        private Timer timer = new Timer();
        private Random rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
        private int Width = 30, Hight = 30, pix = 20;
        private List<coord> snake = new List<coord>();
        private coord apple; 
        private int way = 0; 
        private int apples = 0; 
        private int stage = 1; 
        public int score = 0; 
        int r = 1;

        public Snake(int difficulty)
        {
            this.Text = "Snake"; 
            this.FormBorderStyle = FormBorderStyle.FixedDialog; 
            this.MaximizeBox = false; 
            this.StartPosition = FormStartPosition.CenterScreen; 
            this.DoubleBuffered = true; 
            int caption_size = SystemInformation.CaptionHeight; 
            int frame_size = SystemInformation.FrameBorderSize.Height; 
            this.Size = new Size(Width * pix + 4 * frame_size, Hight * pix + caption_size + 4 * frame_size);
            this.Paint += new PaintEventHandler(Program_Paint); // привязываем обработчик прорисовки формы
            this.KeyDown += new KeyEventHandler(Program_KeyDown); // привязываем обработчик нажатий на кнопки

            timer.Interval = 50 * difficulty; // таймер срабатывает раз в 50*сложность милисекунд
            timer.Tick += new EventHandler(timer_Tick); // привязываем обработчик таймера
            timer.Start(); // запускаем таймер

            // делаем змею из трех сегментов, с начальными координатами внизу и по-центру формы
            snake.Add(new coord(Width / 2, Hight - 3));
            snake.Add(new coord(Width / 2, Hight - 2));
            snake.Add(new coord(Width / 2, Hight - 1));

            apple = new coord(rand.Next(Width), rand.Next(Hight)); 
        }
        // обработка нажатий на клавиши(здесь только стрелки)
        // меняем направление движения, если оно не противоположное
        void Program_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Up:
                    if (way != 2)
                        way = 0;
                    break;
                case Keys.Right:
                    if (way != 3)
                        way = 1;
                    break;
                case Keys.Down:
                    if (way != 0)
                        way = 2;
                    break;
                case Keys.Left:
                    if (way != 1)
                        way = 3;
                    break;
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            // запоминаем координаты головы змеи
            int x = snake[0].X, y = snake[0].Y;
            // в зависимости от направления вычисляем где будет голова на следующем шаге
            switch (way)
            {
                case 0:
                    y--;
                    if (y < 0)
                        y = Hight - 1;
                    break;
                case 1:
                    x++;
                    if (x >= Width)
                        x = 0;
                    break;
                case 2:
                    y++;
                    if (y >= Hight)
                        y = 0;
                    break;
                case 3:
                    x--;
                    if (x < 0)
                        x = Width - 1;
                    break;
            }
            //цикл отвечающий за перемещения тела змеи по координатам
            for (int k = 0; k <= snake.Count() - 1; k++)
            {
                
                if (k != (snake.Count() - 1))
                {
                    snake[snake.Count() - 1 - k].X = snake[snake.Count() - k - 2].X;
                    snake[snake.Count() - 1 - k].Y = snake[snake.Count() - k - 2].Y;
                    //условие проигрыша
                    if (snake[snake.Count() - 1 - k].X == x && snake[snake.Count() - 1 - k].Y == y)
                        Close();
                }
                else
                {
                    snake[0].X = x; snake[0].Y = y;
                }



            }
            //Условие сбора яблока
            if (snake[0].X == apple.X && snake[0].Y == apple.Y) 
            {
                apple = new coord(rand.Next(Width), rand.Next(Hight)); 
                apples++; 
                score += stage; 
                coord c = new coord(snake[snake.Count() - 1].X, snake[snake.Count() - 1].Y);
                snake.Insert(snake.Count(), c);
                if (apples % 10 == 0) 
                {
                    stage++; 
                    timer.Interval -= 10; 
                }
            }
            Invalidate(); // перерисовываем, т.е. идет вызов Program_Paint
        }

        //  прорисовка всех элементов
        void Program_Paint(object sender, PaintEventArgs e)
        {
            Image Fon = Image.FromFile("Fon.png");
            Image Headzmei = Image.FromFile("head.png");
            Image Yabloko = Image.FromFile("yabloko.png");
            Image Telo1 = Image.FromFile("telo1.png");
            Image Telo2 = Image.FromFile("telo2.png");
            Image Telo3 = Image.FromFile("telo3.png");
            e.Graphics.DrawImage(Fon, new Rectangle(0,0, Hight*pix, Width*pix));
            //прорисовка яблок
            e.Graphics.DrawImage(Yabloko, new Rectangle(apple.X * pix, apple.Y * pix, pix, pix));
            //прорисовка хвоста змеи
            if (snake[snake.Count - 2].Y - snake[snake.Count - 1].Y == 1 || snake[snake.Count - 2].Y - snake[snake.Count - 1].Y == -29)
                e.Graphics.DrawImage(Telo1, new Rectangle(snake[snake.Count - 1].X * pix, snake[snake.Count - 1].Y * pix, pix, pix));
            if (snake[snake.Count - 2].Y - snake[snake.Count - 1].Y == -1 || snake[snake.Count - 2].Y - snake[snake.Count - 1].Y == 29)
            {
                Telo1.RotateFlip(RotateFlipType.Rotate180FlipNone);
                e.Graphics.DrawImage(Telo1, new Rectangle(snake[snake.Count - 1].X * pix, snake[snake.Count - 1].Y * pix, pix, pix));
            }
            if (snake[snake.Count - 2].X - snake[snake.Count - 1].X == -1 || snake[snake.Count - 2].X - snake[snake.Count - 1].X == 29)
            {
                Telo1.RotateFlip(RotateFlipType.Rotate90FlipNone);
                e.Graphics.DrawImage(Telo1, new Rectangle(snake[snake.Count - 1].X * pix, snake[snake.Count - 1].Y * pix, pix, pix));
            }
            if (snake[snake.Count - 2].X - snake[snake.Count - 1].X == 1 || snake[snake.Count - 2].X - snake[snake.Count - 1].X == -29)
            {
                Telo1.RotateFlip(RotateFlipType.Rotate270FlipNone);
                e.Graphics.DrawImage(Telo1, new Rectangle(snake[snake.Count - 1].X * pix, snake[snake.Count - 1].Y * pix, pix, pix));
            }

            //прорисовка тела змеи
            for (int i = 1; i < snake.Count - 1; i++)
            {

                if (snake[i].X == snake[i - 1].X && snake[i].X == snake[i + 1].X)
                {
                    if (r % 2 == 1)
                    {
                        Telo2.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    }
                    Telo2.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    e.Graphics.DrawImage(Telo2, new Rectangle(snake[i].X * pix, snake[i].Y * pix, pix, pix));
                }
                else
                {
                    if (snake[i].Y == snake[i - 1].Y && snake[i].Y == snake[i + 1].Y)
                    {
                        if (r % 2 == 1)
                        {
                            Telo2.RotateFlip(RotateFlipType.Rotate180FlipNone);
                        }
                        Telo2.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        e.Graphics.DrawImage(Telo2, new Rectangle(snake[i].X * pix, snake[i].Y * pix, pix, pix));
                        Telo2.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    }
                    else
                    {
                        if ((snake[i].X - snake[i - 1].X == 1 && snake[i].Y - snake[i + 1].Y == 1)
                            || (snake[i].X - snake[i + 1].X == 1 && snake[i].Y - snake[i - 1].Y == 1)
                            || (snake[i].X - snake[i + 1].X == -29 && snake[i].Y - snake[i - 1].Y == 1)
                            || (snake[i].X - snake[i - 1].X == -29 && snake[i].Y - snake[i + 1].Y == 1)
                            || (snake[i].X - snake[i + 1].X == 1 && snake[i].Y - snake[i - 1].Y == -29)
                            || (snake[i].X - snake[i - 1].X == 1 && snake[i].Y - snake[i + 1].Y == -29)
                            || (snake[i].X - snake[i + 1].X == -29 && snake[i].Y - snake[i - 1].Y == -29)
                            || (snake[i].X - snake[i - 1].X == -29 && snake[i].Y - snake[i + 1].Y == -29))
                        {
                            e.Graphics.DrawImage(Telo3, new Rectangle(snake[i].X * pix, snake[i].Y * pix, pix, pix));
                        }
                        if ((snake[i].X - snake[i - 1].X == -1 && snake[i].Y - snake[i + 1].Y == -1)
                            || (snake[i].X - snake[i + 1].X == -1 && snake[i].Y - snake[i - 1].Y == -1)
                            || (snake[i].X - snake[i + 1].X == 29 && snake[i].Y - snake[i - 1].Y == -1)
                            || (snake[i].X - snake[i - 1].X == 29 && snake[i].Y - snake[i + 1].Y == -1)
                            || (snake[i].X - snake[i + 1].X == -1 && snake[i].Y - snake[i - 1].Y == 29)
                            || (snake[i].X - snake[i - 1].X == -1 && snake[i].Y - snake[i + 1].Y == 29)
                            || (snake[i].X - snake[i + 1].X == 29 && snake[i].Y - snake[i - 1].Y == 29)
                            || (snake[i].X - snake[i - 1].X == 29 && snake[i].Y - snake[i + 1].Y == 29))
                        {
                            Telo3.RotateFlip(RotateFlipType.Rotate180FlipNone);
                            e.Graphics.DrawImage(Telo3, new Rectangle(snake[i].X * pix, snake[i].Y * pix, pix, pix));
                            Telo3.RotateFlip(RotateFlipType.Rotate180FlipNone);
                        }
                        if ((snake[i].X - snake[i - 1].X == 1 && snake[i].Y - snake[i + 1].Y == -1)
                            || (snake[i].X - snake[i + 1].X == 1 && snake[i].Y - snake[i - 1].Y == -1)
                            || (snake[i].X - snake[i + 1].X == -29 && snake[i].Y - snake[i - 1].Y == -1)
                            || (snake[i].X - snake[i - 1].X == -29 && snake[i].Y - snake[i + 1].Y == -1)
                            || (snake[i].X - snake[i + 1].X == 1 && snake[i].Y - snake[i - 1].Y == 29)
                            || (snake[i].X - snake[i - 1].X == 1 && snake[i].Y - snake[i + 1].Y == 29)
                            || (snake[i].X - snake[i + 1].X == -29 && snake[i].Y - snake[i - 1].Y == 29)
                            || (snake[i].X - snake[i - 1].X == -29 && snake[i].Y - snake[i + 1].Y == 29))
                        {
                            Telo3.RotateFlip(RotateFlipType.Rotate270FlipNone);
                            e.Graphics.DrawImage(Telo3, new Rectangle(snake[i].X * pix, snake[i].Y * pix, pix, pix));
                            Telo3.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        }
                        if ((snake[i].X - snake[i - 1].X == -1 && snake[i].Y - snake[i + 1].Y == 1)
                            || (snake[i].X - snake[i + 1].X == -1 && snake[i].Y - snake[i - 1].Y == 1)
                            || (snake[i].X - snake[i + 1].X == 29 && snake[i].Y - snake[i - 1].Y == 1)
                            || (snake[i].X - snake[i - 1].X == 29 && snake[i].Y - snake[i + 1].Y == 1)
                            || (snake[i].X - snake[i + 1].X == -1 && snake[i].Y - snake[i - 1].Y == -29)
                            || (snake[i].X - snake[i - 1].X == -1 && snake[i].Y - snake[i + 1].Y == -29)
                            || (snake[i].X - snake[i + 1].X == 29 && snake[i].Y - snake[i - 1].Y == -29)
                            || (snake[i].X - snake[i - 1].X == 29 && snake[i].Y - snake[i + 1].Y == -29))
                        {
                            Telo3.RotateFlip(RotateFlipType.Rotate90FlipNone);
                            e.Graphics.DrawImage(Telo3, new Rectangle(snake[i].X * pix, snake[i].Y * pix, pix, pix));
                            Telo3.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        }
                    }
                }
                r++;
            }
            //прорисовка путей головы
            switch (way)
            {
                case 0:
                    e.Graphics.DrawImage(Headzmei, new Rectangle(snake[0].X * pix, snake[0].Y * pix, pix, pix));
                    break;
                case 1:
                    Headzmei.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    e.Graphics.DrawImage(Headzmei, new Rectangle(snake[0].X * pix, snake[0].Y * pix, pix, pix));
                    break;
                case 2:
                    Headzmei.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    e.Graphics.DrawImage(Headzmei, new Rectangle(snake[0].X * pix, snake[0].Y * pix, pix, pix));
                    break;
                case 3:
                    Headzmei.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    e.Graphics.DrawImage(Headzmei, new Rectangle(snake[0].X * pix, snake[0].Y * pix, pix, pix));
                    break;
            }
            string state = "Apples:" + apples.ToString() + "\n" +
                "Stage:" + stage.ToString() + "\n" + "Score:" + score.ToString();
            e.Graphics.DrawString(state, new Font("Arial", 10, FontStyle.Italic), Brushes.Black, new Point(5, 5));
        }
    }
}
