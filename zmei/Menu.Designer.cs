﻿namespace zmei
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Scoreboard = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.Newgame = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Scoreboard
            // 
            this.Scoreboard.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Scoreboard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Scoreboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Scoreboard.Location = new System.Drawing.Point(86, 121);
            this.Scoreboard.Name = "Scoreboard";
            this.Scoreboard.Size = new System.Drawing.Size(98, 35);
            this.Scoreboard.TabIndex = 1;
            this.Scoreboard.Text = "Рекорды";
            this.Scoreboard.UseVisualStyleBackColor = false;
            this.Scoreboard.Click += new System.EventHandler(this.Scoreboard_Click);
            // 
            // Exit
            // 
            this.Exit.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit.Location = new System.Drawing.Point(86, 177);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(98, 35);
            this.Exit.TabIndex = 2;
            this.Exit.Text = "Выход";
            this.Exit.UseVisualStyleBackColor = false;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // Newgame
            // 
            this.Newgame.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Newgame.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Newgame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Newgame.Location = new System.Drawing.Point(86, 67);
            this.Newgame.Name = "Newgame";
            this.Newgame.Size = new System.Drawing.Size(98, 35);
            this.Newgame.TabIndex = 3;
            this.Newgame.Text = "Начать игру";
            this.Newgame.UseVisualStyleBackColor = false;
            this.Newgame.Click += new System.EventHandler(this.Newgame_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.Newgame);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.Scoreboard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Змейка";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button Scoreboard;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button Newgame;
    }
}