﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zmei
{
    public partial class Sco : Form
    {
        public Sco()
        {
            InitializeComponent();
            using (BinaryReader reader=new BinaryReader(File.Open(Application.StartupPath + @"\Score.dat", FileMode.OpenOrCreate)))
            {
                while (reader.PeekChar() > -1)
                {
                    string name = reader.ReadString();
                    string diff = reader.ReadString();
                    int sco = reader.ReadInt32();
                    dataSco.Rows.Add(name, diff, sco);
                    
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
