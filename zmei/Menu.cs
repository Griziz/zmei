﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zmei
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }
        private void Newgame_Click(object sender, EventArgs e)
        {
            Hide();
            Difficult dif = new Difficult();
            dif.ShowDialog();
            Show();
        }
        private void Scoreboard_Click(object sender, EventArgs e)
        {
            Hide();
            Sco sco = new Sco();
            sco.ShowDialog();
            Show();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
