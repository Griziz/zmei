﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zmei
{
    public partial class Score : Form
    {
        string dif;
        int sco;
        public Score(int score, string diffic)
        {
            InitializeComponent();
            string s = score.ToString();
            label2.Text =label2.Text+ s;
            dif = diffic;
            sco = score;
        }


        private void SaveScore_Click(object sender, EventArgs e)
        {
            List<Scores> easy = new List<Scores>();
            List<Scores> norm = new List<Scores>();
            List<Scores> hard = new List<Scores>();
            List<Scores> inse = new List<Scores>();
            using (BinaryReader reader = new BinaryReader(File.Open(Application.StartupPath + @"\Score.dat", FileMode.OpenOrCreate)))
            {
                while (reader.PeekChar() > -1)
                {
                    string name = reader.ReadString();
                    string diff = reader.ReadString();
                    int sco = reader.ReadInt32();
                    Scores tm = new Scores(name, diff, sco);
                    switch (diff)
                    {
                        case "Легкий":
                            easy.Add(tm);
                            break;
                        case "Нормальный":
                            norm.Add(tm);
                            break;
                        case "Сложный":
                            hard.Add(tm);
                            break;
                        case "Сложнейший":
                            inse.Add(tm);
                            break;
                    }
                }
            }
            Scores tmp = new Scores(textBox1.Text, dif, sco);
            switch (dif)
            {

                case "Легкий":
                    easy.Add(tmp);
                    easy.Sort((x, y) => y.Scor.CompareTo(x.Scor));
                    break;
                case "Нормальный":
                    norm.Add(tmp);
                    norm.Sort((x, y) => y.Scor.CompareTo(x.Scor));
                    break;
                case "Сложный":
                    hard.Add(tmp);
                    hard.Sort((x, y) => y.Scor.CompareTo(x.Scor));
                    break;
                case "Сложнейший":
                    inse.Add(tmp);
                    inse.Sort((x, y) => y.Scor.CompareTo(x.Scor));
                    break;
            }

            using (BinaryWriter writer = new BinaryWriter(File.Open(Application.StartupPath + @"\Score.dat", FileMode.Create)))
            {

                foreach (Scores element in inse)
                {
                    writer.Write(element.Name);
                    writer.Write(element.Diffic);
                    writer.Write(element.Scor);
                }
                foreach (Scores element in hard)
                {
                    writer.Write(element.Name);
                    writer.Write(element.Diffic);
                    writer.Write(element.Scor);
                }
                foreach (Scores element in norm)
                {
                    writer.Write(element.Name);
                    writer.Write(element.Diffic);
                    writer.Write(element.Scor);
                }
                foreach (Scores element in easy)
                {
                    writer.Write(element.Name);
                    writer.Write(element.Diffic);
                    writer.Write(element.Scor);
                }
            }
            Close();
        }
    }
}
