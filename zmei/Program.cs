﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
namespace zmei
{
   

    public class Program : Form
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new Menu());
        }

        //класс для удобства работы с координатами яблока и сегментов змеи
    }
    public class Scores
    {
        public string Name;
        public string Diffic;
        public int Scor;
        public Scores(string name, string diffic, int scor)
        {
            Name = name;
            Diffic = diffic;
            Scor = scor;
        }
        public int Comparer(int a, int b)
        {
            if (a > b)
                return 1;
            else
                if (a < b) { return -1; }
            return 0;
        }
    }
}
