﻿CREATE TABLE [dbo].[Table]
(
	[Id] int NOT NULL DEFAULT NEWID(),
	[Имя] NVARCHAR(50) NULL , 
    [Сложность] NVARCHAR(50) NULL, 
    [Очки] INT NULL, 
    CONSTRAINT [PK_Table] PRIMARY KEY ([Id])
)
