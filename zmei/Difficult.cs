﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zmei
{
    public partial class Difficult : Form
    {
        public Difficult()
        {
            InitializeComponent();
        }
        private void start(int x)
        {
            Hide();
            Snake snake = new Snake(x);
            snake.ShowDialog();
            int score = snake.score;
            string str=null;
           
            switch (x)
            {
                case 4:
                    str = "Легкий";
                    break;
                case 3:
                    str = "Нормальный";
                    break;
                case 2:
                    str = "Сложный";
                    break;
                case 1:
                    str = "Сложнейший";
                    break;

            }
            Score scr = new Score(score,str);
            scr.ShowDialog();
            Close();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            start(4);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            start(3);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            start(2);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            start(1);
        }
    }
}
